package br.com.xfoodapp.xfood.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.model.Restaurant;
import br.com.xfoodapp.xfood.util.Util;

/**
 * Created by Victorvrp on 17/03/2016.
 */
public class RestaurantsListAdapter extends BaseAdapter {
    /**
     * Class attributes
     * @return
     */
    private LayoutInflater mInflater;
    private ArrayList<Restaurant> mData = new ArrayList<Restaurant>();
    private Context ctx;

    public RestaurantsListAdapter(Context ctx, ArrayList<Restaurant> mData) {
        this.ctx = ctx;
        this.mData = mData;
        mInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Restaurant getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.adapter_restaurant_item, null);
            holder.name = (TextView) convertView.findViewById(R.id.restaurantName);
            holder.image = (ImageView) convertView.findViewById(R.id.restaurantImage);
            holder.distance = (TextView) convertView.findViewById(R.id.restaurantDistance);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        Restaurant restaurant = mData.get(position);
        holder.name.setText(restaurant.getName());
        holder.distance.setText(restaurant.getDistanceText());
        Glide.with(ctx).load(restaurant.getProfileImage()).into(holder.image);

        return convertView;
    }

    class ViewHolder {
        public ImageView image;
        public TextView name;
        public TextView distance;
    }
}
