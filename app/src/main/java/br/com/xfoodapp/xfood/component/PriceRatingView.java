package br.com.xfoodapp.xfood.component;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joanzapata.iconify.widget.IconTextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victorvrp on 19/03/2016.
 */
public class PriceRatingView {
    /**
     * Layout attributes
     */
    private Context ctx;
    private int media;

    public PriceRatingView(Context ctx, int media) {
        this.ctx = ctx;
        this.media = media;
    }

    private List<TextView> getStars() {
        int restantMedia = media;
        List<TextView> textViewList = new ArrayList<TextView>();
        IconTextView textView;

        for (int i = 0; i < 5; i++) {
            textView = new IconTextView(ctx);

            if (restantMedia >= 1) {
                textView.setText("{fa-usd}");
                textView.setTextColor(Color.parseColor("#aaaaaa"));
            } else {
                textView.setText("{fa-usd}");
                textView.setTextColor(Color.parseColor("#e1e1e1"));
            }

            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);

            // layout params
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 5, 0);
            textView.setLayoutParams(params);

            // add to list
            textViewList.add(textView);

            // decrement
            restantMedia--;
        }

        return textViewList;
    }

    public void setIconsInto(LinearLayout container) {
        List<TextView> textViewList = getStars();

        for (TextView textView : textViewList) {
            container.addView(textView);
        }
    }
}
