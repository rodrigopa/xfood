package br.com.xfoodapp.xfood.model;

import java.text.DecimalFormat;

import br.com.xfoodapp.xfood.http.ApiRoutes;

/**
 * Created by Victorvrp on 17/03/2016.
 */
public class Restaurant {
    private int id;
    private String name;
    private int priceRating;
    private Double qualityRating;
    private String baseImage;
    private Double distance;
    private boolean hasFrontImage;

    public Restaurant(int id, String name, int priceRating, Double qualityRating, Double distance) {
        this.id = id;
        this.name = name;
        this.priceRating = priceRating;
        this.qualityRating = qualityRating;
        this.distance = distance;
        this.hasFrontImage = false;

        // setting image url
        this.baseImage = ApiRoutes.UPLOAD + "restaurants/" + this.id + "/profile/";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPriceRating() {
        return priceRating;
    }

    public void setPriceRating(int priceRating) {
        this.priceRating = priceRating;
    }

    public Double getQualityRating() {
        return qualityRating;
    }

    public void setQualityRating(Double qualityRating) {
        this.qualityRating = qualityRating;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getDistanceText() {
        Double kmDistance = distance / 0.3959;
        DecimalFormat df = new DecimalFormat("#.0");
        return df.format(kmDistance) + " km";
    }

    public void setHasFrontImage(boolean hasFrontImage) {
        this.hasFrontImage = hasFrontImage;
    }

    /**
     * Get Restaurants images
     * @return
     */
    public String getProfileImage() {
        return baseImage + "medium.png";
    }
    public String getFrontImage() {
        if (!hasFrontImage) {
            return null;
        }

        return ApiRoutes.UPLOAD + "restaurants/" + this.id + "/front/medium.png";
    }
}
