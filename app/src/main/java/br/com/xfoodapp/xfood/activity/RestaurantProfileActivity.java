package br.com.xfoodapp.xfood.activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.nineoldandroids.view.ViewHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.component.PriceRatingView;
import br.com.xfoodapp.xfood.component.StarRatingView;
import br.com.xfoodapp.xfood.http.ApiRoutes;
import br.com.xfoodapp.xfood.http.JsonHandler;
import br.com.xfoodapp.xfood.http.rest.RestaurantsHttp;
import br.com.xfoodapp.xfood.model.Restaurant;
import br.com.xfoodapp.xfood.util.Util;
import cz.msebera.android.httpclient.Header;

public class RestaurantProfileActivity extends BaseActivity {
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private Dialog loadingDialog;
    private int restaurantId;

    /**
     * Layout components
     */
    private ImageView image;
    private TextView name;
    private TextView address;
    private ImageView logo;
    private LinearLayout containerStarRating;
    private LinearLayout containerPriceRating;
    private TextView starRatingMedia;
    private StarRatingView starRatingView;
    private PriceRatingView priceRatingView;
    private Button maps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_profile);

        // save context
        this.ctx = this;

        // setup
        setupToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        // get data from intent
        Intent intent = getIntent();
        restaurantId = intent.getIntExtra("id", -1);

        if (restaurantId == -1) {
            alertNotFound();
            return;
        }

        // loading dialog
        loadingDialog = Util.loadingDialog(this);
        loadingDialog.show();

        // requiring the restaurant
        RestaurantsHttp http = new RestaurantsHttp(ctx);
        http.getRestaurant(restaurantId, new JsonHandler(ctx) {
            public void onFinish() {
                loadingDialog.hide();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                updateRestaurant(response);
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                super.onFailure(statusCode, headers, throwable, errorResponse);

                String error = Util.getError(errorResponse);
                if (error != null) {
                    alert("Erro!", error);
                }
            }

        });
    }

    private void updateRestaurant(JSONObject response) {
        try {
            // create new object
            final String restaurantName = response.getString("name");
            int priceRating = response.getInt("price_rating");
            String frontImage = ApiRoutes.UPLOAD + "restaurants/" + restaurantId + "/front/medium.png";
            String profileImage =  ApiRoutes.UPLOAD + "restaurants/" + restaurantId + "/profile/medium.png";

            logo = (ImageView) findViewById(R.id.restaurantLogo);
            image = (ImageView) findViewById(R.id.restaurantImage);
            name = (TextView) findViewById(R.id.restaurantName);
            address = (TextView) findViewById(R.id.restaurantAdddress);
            containerStarRating = (LinearLayout) findViewById(R.id.containerStarRating);
            containerPriceRating = (LinearLayout) findViewById(R.id.containerPriceRating);
            starRatingMedia = (TextView) findViewById(R.id.starRatingMedia);
            priceRatingView = new PriceRatingView(ctx, priceRating);
            maps = (Button) findViewById(R.id.maps);

            // set name to components
            name.setText(restaurantName);
            getSupportActionBar().setTitle(restaurantName);
            collapsingToolbarLayout.setTitle(restaurantName);
            getCollapsingTextView();

            // address
            address.setText(getCompleteAddress(response.getJSONObject("address")));

            if (response.get("quality_rating") != null) {
                starRatingView = new StarRatingView(ctx, response.getDouble("quality_rating"));
                starRatingMedia.setText(starRatingView.getValue());
                starRatingView.setIconsInto(containerStarRating);
            }

            priceRatingView.setIconsInto(containerPriceRating);

            if (response.getBoolean("has_front_image")) {
                Glide.with(ctx).load(frontImage).centerCrop().into(image);
            } else {
                Glide.with(ctx).load(profileImage).into(image);
            }

            Glide.with(ctx)
                    .load(profileImage)
                    .into(logo);

            // Get address
            JSONObject address = response.getJSONObject("address");
            final String latitude = address.getString("latitude");
            final String longitude = address.getString("longitude");

            // Botão como chegar
            maps.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String uri = String.format(Locale.ENGLISH, "geo:0,0?q=") +
                            android.net.Uri.encode(String.format("%s@%s,%s", restaurantName, latitude, longitude), "UTF-8");

                    final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            alertNotFound();
        }
    }

    private void alertNotFound() {
        new AlertDialog.Builder(this)
                .setTitle("Erro!")
                .setMessage("O restaurante não existe!")
                .setPositiveButton("OK", null)
                .show();
    }

    private void getCollapsingTextView() {
        TextView toolbarTitle = null;
        for (int i = 0; i < toolbar.getChildCount(); ++i) {
            View child = toolbar.getChildAt(i);

            // assuming that the title is the first instance of TextView
            // you can also check if the title string matches
            if (child instanceof TextView) {
                toolbarTitle = (TextView)child;
                break;
            }
        }

        toolbarTitle.setVisibility(View.GONE);
    }

    private String getCompleteAddress(JSONObject object) {
        String address = "";

        try {
            JSONObject district = object.getJSONObject("district");
            JSONObject city = object.getJSONObject("city");
            JSONObject state = city.getJSONObject("state");

            address = object.getString("street") + ", " + object.getString("number") +
                        ", " + district.getString("name") + "\n" +
                    "CEP: " + object.getString("cep") + "\n" +
                    city.getString("name") + " - " + state.getString("uf");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return address;
    }
}
