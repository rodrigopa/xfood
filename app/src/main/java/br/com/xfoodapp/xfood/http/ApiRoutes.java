package br.com.xfoodapp.xfood.http;

/**
 * Created by Victorvrp on 17/03/2016.
 */
public class ApiRoutes {
    public static final String BASE_APP = "http://192.168.25.132/xfood/public/";
    public static final String BASE_API = BASE_APP + "api/";
    public static final String LOGIN = BASE_API + "user/login";
    public static final String SOCIAL_LOGIN = BASE_API + "user/social/login";
    public static final String REGISTER = BASE_API + "user/register";
    public static final String LIST_RESTAURANTS = BASE_API + "restaurant";
    public static final String UPLOAD = BASE_APP + "upload/";
    public static final String USER_ME = BASE_API + "me";
    public static final String RESTAURANT_SHOW = BASE_API + "restaurant/";
}
