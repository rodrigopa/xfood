package br.com.xfoodapp.xfood.http;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONObject;

import br.com.xfoodapp.xfood.activity.LoginActivity;
import br.com.xfoodapp.xfood.controller.SessionController;
import br.com.xfoodapp.xfood.util.Util;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Victorvrp on 17/03/2016.
 */
public class JsonHandler extends JsonHttpResponseHandler {
    /**
     * Class attributes
     */
    private Context mContext;

    public JsonHandler(Context ctx) {
        // save context
        this.mContext = ctx;
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
        if (statusCode == 401) {
            checkTokenError(errorResponse);
        } else {
            Toast.makeText(mContext, throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String response, Throwable throwable) {
        throwable.printStackTrace();
        Toast.makeText(mContext, throwable.getMessage(), Toast.LENGTH_SHORT).show();
    }

    /**
     * Verificar a existência de erro na resposta de uma requisição errada.
     * @param errorResponse
     */
    private void checkTokenError(JSONObject errorResponse) {
        String error = Util.getError(errorResponse);
        if (error != null) {
            // remove token
            SessionController sessionController = new SessionController(mContext);
            sessionController.removeToken();

            // create toast to message
            Toast.makeText(mContext, "Sua sessão expirou!\nEntre novamente!", Toast.LENGTH_SHORT).show();

            // close all activities and start the login activity
            Intent intent = new Intent(mContext, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            mContext.startActivity(intent);
        }
    }

    /**
     * AlertDialog para erros
     * @param title
     * @param message
     */
    public void alert(String title, String message) {
        new AlertDialog.Builder(mContext)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton("OK", null)
                .show();
    }
}
