package br.com.xfoodapp.xfood.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import org.json.JSONObject;

import java.util.Arrays;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.util.ApplicationOptions;
import br.com.xfoodapp.xfood.util.Util;

public class RegisterTypeActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    /**
     * Layout components
     */
    private Button facebookRegister;
    private Button googleRegister;
    private Button emailRegister;
    private RelativeLayout rootLayout;

    /**
     * Class attributes
     */
    private CallbackManager callbackManager;

    // google plus
    private boolean mIntentInProgress;
    private boolean signedInUser;
    private ConnectionResult mConnectionResult;
    private GoogleApiClient mGoogleApiClient;
    private static final int RC_SIGN_IN = 0;

    // get account permission
    private static final int INITIAL_REQUEST = 0;
    private static final String[] ACCOUNT_PERMS = { Manifest.permission.GET_ACCOUNTS };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // facebook sdk start
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        // logout facebook user if logged
        LoginManager.getInstance().logOut();

        setContentView(R.layout.activity_register_type);

        // setup toolbar
        setupToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // save context
        this.ctx = this;

        // register layout components
        facebookRegister = (Button) findViewById(R.id.facebook_register);
        googleRegister = (Button) findViewById(R.id.google_register);
        emailRegister = (Button) findViewById(R.id.email_register);
        rootLayout = (RelativeLayout) findViewById(R.id.rootLayout);

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Intent intent = new Intent(ctx, RegisterActivity.class);
                intent.putExtra("type", ApplicationOptions.FACEBOOK_LOGIN_TYPE);
                startActivity(intent);
            }

            @Override
            public void onCancel() {
                Snackbar snackbar = Snackbar.make(rootLayout, "Você não nos deu permissão de acessar seus dados :(", Snackbar.LENGTH_LONG);
                snackbar.show();
            }

            @Override
            public void onError(FacebookException e) {
                Snackbar snackbar = Snackbar.make(rootLayout, "Ocorreu algum erro ao registrar com o facebook!", Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        });

        // Initializing google plus api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

        facebookRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(((RegisterTypeActivity) ctx), Arrays.asList("public_profile, email"));
            }
        });

        googleRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googlePlusLogin();
            }
        });

        emailRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, RegisterActivity.class);
                intent.putExtra("type", ApplicationOptions.EMAIL_LOGIN_TYPE);
                startActivity(intent);
            }
        });
    }

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }

        if (!mIntentInProgress) {
            // store mConnectionResult
            mConnectionResult = result;

            if (signedInUser) {
                resolveSignInError();
            }
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle arg0) {
        if (signedInUser) {
            signedInUser = false;
            // start new activity
            Intent intent = new Intent(ctx, RegisterActivity.class);
            intent.putExtra("type", ApplicationOptions.GOOGLE_LOGIN_TYPE);
            startActivity(intent);
        } else {
            googlePlusLogout();
        }
    }

    private boolean canAccessAccounts() {
        return (hasPermission(Manifest.permission.GET_ACCOUNTS));
    }

    private boolean hasPermission(String perm) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode) {
            case INITIAL_REQUEST:
                if (!canAccessAccounts()) {
                    final Snackbar snackBar = Snackbar.make(rootLayout, "Você não nos deu permissão para acessar sua conta do Google :(", Snackbar.LENGTH_LONG);
                    snackBar.setAction("Tentar denovo", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snackBar.dismiss();
                            googlePlusLogin();
                        }
                    });
                    snackBar.show();
                } else {
                    googlePlusLogin();
                }

                break;
        }
    }

    private void googlePlusLogin() {
        if (canAccessAccounts()) {
            if (!mGoogleApiClient.isConnecting()) {
                signedInUser = true;
                resolveSignInError();
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(ACCOUNT_PERMS, INITIAL_REQUEST);
            }
        }
    }

    private void googlePlusLogout() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            mIntentInProgress = false;
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }

        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

}
