package br.com.xfoodapp.xfood.activity;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.util.Util;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // timeout
        new Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        Class clazz;
                        if (Util.getTokenAccess() != null) {
                            clazz = HomeActivity.class;
                        } else {
                            clazz = LoginActivity.class;
                        }

                        startActivity(new Intent(SplashActivity.this, clazz));
                    }
                },
                800);
    }
}
