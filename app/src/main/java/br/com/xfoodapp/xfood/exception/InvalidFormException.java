package br.com.xfoodapp.xfood.exception;

import android.widget.EditText;

/**
 * Created by Victorvrp on 16/03/2016.
 */
public class InvalidFormException extends Exception {
    /**
     * Input
     */
    private EditText etElem;

    /**
     * Constructors
     */
    public InvalidFormException() {}
    public InvalidFormException(String message, EditText formElement) {
        super(message);
        this.etElem = formElement;
    }

    public EditText getElem() {
        return etElem;
    }
}
