package br.com.xfoodapp.xfood.http.rest;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.activity.CompleteRegisterActivity;
import br.com.xfoodapp.xfood.activity.HomeActivity;
import br.com.xfoodapp.xfood.activity.LoginActivity;
import br.com.xfoodapp.xfood.http.ApiRoutes;
import br.com.xfoodapp.xfood.http.JsonHandler;
import br.com.xfoodapp.xfood.model.User;
import br.com.xfoodapp.xfood.util.ApplicationOptions;
import br.com.xfoodapp.xfood.util.Util;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Victorvrp on 18/03/2016.
 */
public class UserHttp extends AuthenticatedHttp {
    public UserHttp(Context ctx) {
        this.ctx = ctx;

        // setup http
        setupClient();
    }

    public void register(String name, String email, String phone,
                         String password, String uid, String register_type, String token, final Dialog loadingDialog) {
        // request params
        RequestParams params = new RequestParams();
        params.add("name", name);
        params.add("email", email);
        params.add("phone", phone);

        if (password != null) {
            params.add("password", password);
        }

        params.add("uid", uid);
        params.add("register_type", register_type);
        params.add("token", token);

        client.post(ctx, ApiRoutes.REGISTER, params, new JsonHandler(ctx) {
            @Override
            public void onFinish() {
                loadingDialog.cancel();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    Intent intent = new Intent(ctx, CompleteRegisterActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    ctx.startActivity(intent);
                } catch (Exception e) {
                    alert("Falha no registro", "Não foi possível realizar o registro, tente novamente!");
                }
            }

            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                ArrayList<String> errorsList = Util.getErrors(errorResponse);
                if (errorsList != null) {
                    String errorsString = TextUtils.join("\n", errorsList);
                    alert("Erro!", errorsString);
                }
            }
        });
    }

    public void profile(final UserHttp.ProfileCallback callback) {
        client = getAuthHeader(client);
        client.get(ApiRoutes.USER_ME, new JsonHandler(ctx) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                try {
                    int id = response.getInt("id");
                    String name = response.getString("name");
                    String uid = response.getString("uid");
                    String email = response.getString("email");
                    int type = response.getInt("type");

                    String image = null;
                    if (type == ApplicationOptions.EMAIL_LOGIN_TYPE) {
                        if (response.getString("image") != null) {
                            image = response.getString("image");
                        }
                    } else if (type == ApplicationOptions.FACEBOOK_LOGIN_TYPE) {
                        image = "http://graph.facebook.com/" + uid + "/picture?width=500&height=500";
                    }

                    User user = new User(id, name, email, type, uid, image);
                    callback.success(user);
                } catch (Exception e) {}
            }
        });
    }


    public interface ProfileCallback {
        void success(User user);
    }
}
