package br.com.xfoodapp.xfood.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.XFoodApplication;
import br.com.xfoodapp.xfood.adapter.NavMenuAdapter;
import br.com.xfoodapp.xfood.controller.SessionController;
import br.com.xfoodapp.xfood.http.rest.UserHttp;
import br.com.xfoodapp.xfood.model.User;

/**
 * Created by Victorvrp on 26/02/2016.
 */
public class BaseActivity extends AppCompatActivity {
    protected DrawerLayout mDrawerLayout;
    protected ActionBarDrawerToggle mDrawerToggle;
    protected Context ctx;
    protected Toolbar toolbar;

    protected void setupToolbar() {
        // toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setElevation(0);
    }

    protected int getActionBarSize() {
        TypedValue typedValue = new TypedValue();
        int[] textSizeAttr = new int[]{R.attr.actionBarSize};
        int indexOfAttrTextSize = 0;
        TypedArray a = obtainStyledAttributes(typedValue.data, textSizeAttr);
        int actionBarSize = a.getDimensionPixelSize(indexOfAttrTextSize, -1);
        a.recycle();
        return actionBarSize;
    }

    protected void setupDrawerLayout() {
        // Drawer layout
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)
        {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
                syncState();
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
                syncState();
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        ListView navList = (ListView) findViewById(R.id.nav_list);
        NavMenuAdapter navMenuAdapter = new NavMenuAdapter(this);
        navMenuAdapter.addItem("Configurações", "{fa-wrench}");
        navMenuAdapter.addItem("Sair", "{fa-sign-out}");
        navList.setAdapter(navMenuAdapter);

        navList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 1:
                        // remove token
                        SessionController sessionController = new SessionController(ctx);
                        sessionController.removeToken();

                        // close all activities and start the login activity
                        Intent intent = new Intent(ctx, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        ctx.startActivity(intent);
                        break;
                }
            }
        });
    }

    protected void updateUserNav() {
        Log.e("BaseActivity", "updateUserNav");
        User user = XFoodApplication.user;

        ImageView profileImage = (ImageView) mDrawerLayout.findViewById(R.id.profile_image);
        TextView name = (TextView) mDrawerLayout.findViewById(R.id.userName);

        if (user.getImage() != null) {
            Glide.with(this)
                    .load(user.getImage())
                    .into(profileImage);
        }
        name.setText(user.getName());
    }

    protected void updateUserData() {
        if (XFoodApplication.user == null) {
            UserHttp userHttp = new UserHttp(ctx);
            userHttp.profile(new UserHttp.ProfileCallback() {
                @Override
                public void success(User user) {
                    XFoodApplication.user = user;
                    updateUserNav();
                }
            });
        } else {
            updateUserNav();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mDrawerToggle != null) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawerToggle != null) {
            mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

}
