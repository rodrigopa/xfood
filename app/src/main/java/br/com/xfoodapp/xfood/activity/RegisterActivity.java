package br.com.xfoodapp.xfood.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.ImageRequest;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.exception.InvalidFormException;
import br.com.xfoodapp.xfood.helpers.Validator;
import br.com.xfoodapp.xfood.http.rest.UserHttp;
import br.com.xfoodapp.xfood.util.ApplicationOptions;
import br.com.xfoodapp.xfood.util.Util;

public class RegisterActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    /**
     * Layout components
     */
    private EditText etName;
    private EditText etEmail;
    private EditText etPhone;
    private EditText etPassword;
    private Button btnRegister;
    private ImageView profileImage;

    /**
     * Class attributes
     */
    private int type = -1;
    private boolean mIntentInProgress;
    private ConnectionResult mConnectionResult;
    private GoogleApiClient mGoogleApiClient;
    private String token;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // getting intent
        Intent intent = getIntent();
        type = intent.getIntExtra("type", -1);

        if (type == -1) {
            Toast.makeText(this, "Não foi possível realizar o registro!", Toast.LENGTH_LONG).show();
            finish();
        }

        setContentView(R.layout.activity_register);

        // setup view
        setupToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // save context
        this.ctx = this;

        // register layout components
        etName = (EditText) findViewById(R.id.name);
        etEmail = (EditText) findViewById(R.id.email);
        etPhone = (EditText) findViewById(R.id.phone);
        etPassword = (EditText) findViewById(R.id.password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        profileImage = (ImageView) findViewById(R.id.profile_image);

        // check for facebook type
        if (type == ApplicationOptions.EMAIL_LOGIN_TYPE) {
            etPassword.setVisibility(View.VISIBLE);
            profileImage.setVisibility(View.GONE);
        } else if (type == ApplicationOptions.FACEBOOK_LOGIN_TYPE) {
            if (AccessToken.getCurrentAccessToken() != null) {
                token = AccessToken.getCurrentAccessToken().getToken();
                GraphRequest request = GraphRequest.newMeRequest(
                        AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject me, GraphResponse response) {
                                if (AccessToken.getCurrentAccessToken() != null) {
                                    if (me != null) {
                                        try {
                                            String profileImageUrl = ImageRequest.getProfilePictureUri(me.optString("id"), 500, 500).toString();
                                            Glide.with(ctx).
                                                    load(profileImageUrl).into(profileImage);

                                            uid = me.getString("id");
                                            etName.setText(me.getString("name"));
                                            etEmail.setText(me.getString("email"));
                                            etEmail.setEnabled(false);
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                            }
                        });

                // set parameters
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email");
                request.setParameters(parameters);
                GraphRequest.executeBatchAsync(request);
            } else {
                Toast.makeText(this, "Erro ao logar com Facebook!", Toast.LENGTH_LONG).show();
                finish();
            }
        } else if (type == ApplicationOptions.GOOGLE_LOGIN_TYPE) {
            // Initializing google plus api client
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                    .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        }

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    // validate form
                    validate();

                    // Fazer a requisição para a API, para autenticar os dados fornecidos pelo usuário
                    submit();
                } catch (InvalidFormException e) {
                    e.getElem().requestFocus();
                    e.getElem().setError(e.getMessage());
                }
            }
        });
    }

    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this, 0).show();
            return;
        }

        if (!mIntentInProgress) {
            // store mConnectionResult
            mConnectionResult = result;
        }
    }

    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle arg0) {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                uid = currentPerson.getId();

                new AsyncTask<String, Void, Void>() {
                    @Override
                    protected Void doInBackground(String... params) {
                        try {
                            String accountEmail = params[0];
                            String scopes = "oauth2:https://www.googleapis.com/auth/userinfo.profile";
                            token = GoogleAuthUtil.getToken(getApplicationContext(), accountEmail, scopes);
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }
                        catch (GoogleAuthException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                }.execute(email);

                String profileImageUrl = currentPerson.getImage().getUrl();
                profileImageUrl = profileImageUrl.substring(0,
                        profileImageUrl.length() - 2)
                        + 500;
                Glide.with(ctx).
                        load(profileImageUrl).into(profileImage);
                etName.setText(personName);
                etEmail.setText(email);
                etEmail.setEnabled(false);
            } else {
                Toast.makeText(this, "Erro ao logar com Google Plus!", Toast.LENGTH_LONG).show();
                finish();
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Validar os dados inseridos pelo usuários nas caixas de texto
     */
    private void validate() throws InvalidFormException {
        String nameValue = etName.getText().toString().trim();
        String emailValue = etEmail.getText().toString().trim();
        String phoneValue = etPhone.getText().toString().trim();
        String passwordValue = etPassword.getText().toString().trim();

        if (nameValue.isEmpty()) {
            throw new InvalidFormException("Informe seu nome!", etName);
        }

        if (!Validator.email(emailValue)) {
            throw new InvalidFormException("Forneça um email válido!", etEmail);
        }

        if (phoneValue.isEmpty()) {
            throw new InvalidFormException("Informe seu telefone!", etPhone);
        }

        if (type == 0 && passwordValue.isEmpty()) {
            throw new InvalidFormException("Informe sua senha!", etPassword);
        }
    }

    /**
     * Enviar os dados para o servidor
     */
    private void submit() {
        String nameValue = etName.getText().toString().trim();
        String emailValue = etEmail.getText().toString().trim();
        String phoneValue = etPhone.getText().toString().trim();
        String passwordValue = null;

        if (etPassword.getVisibility() == View.VISIBLE) {
            passwordValue = etPassword.getText().toString().trim();;
        }

        // dialog
        Dialog dialog = Util.loadingDialog(this);
        dialog.show();

        Log.e("teste", String.valueOf(type));

        // http request
        UserHttp http = new UserHttp(this);
        http.register(nameValue, emailValue, phoneValue, passwordValue, uid, String.valueOf(type), token, dialog);
    }
}
