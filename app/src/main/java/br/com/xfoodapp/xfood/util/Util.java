package br.com.xfoodapp.xfood.util;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Environment;
import android.view.Window;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;

import br.com.xfoodapp.xfood.BuildConfig;
import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.XFoodApplication;
import br.com.xfoodapp.xfood.controller.SessionController;

/**
 * Created by Victorvrp on 17/03/2016.
 */
public class Util {
    public static final String FONT_ICON = "fontawesome-webfont.ttf";
    public static void setFont(Context ctx, String fontPath, TextView... textViews) {
        Typeface tf = Typeface.createFromAsset(ctx.getAssets(), fontPath);

        for (TextView textView : textViews) {
            textView.setTypeface(tf);
        }
    }

    public static Bitmap resizeBitmap(Context ctx, int resId, int width, int height) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(ctx.getResources(), resId);
        Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, width, height, false);
        return resizedBitmap;
    }

    public static boolean checkError(JSONObject jsonObject) {
        try {
            return jsonObject.getBoolean("error");
        } catch (Exception e) {
            return false;
        }
    }

    public static String getError(JSONObject jsonObject) {
        try {
            if (jsonObject.has("error")) {
                return jsonObject.getString("error");
            }
        } catch (Exception e) {
            return null;
        }

        return null;
    }

    public static ArrayList<String> getErrors(JSONObject jsonObject) {
        try {
            if (jsonObject.getJSONArray("errors") != null) {
                ArrayList<String> listdata = new ArrayList<String>();
                JSONArray jArray = jsonObject.getJSONArray("errors");
                if (jArray != null) {
                    for (int i=0;i<jArray.length();i++){
                        listdata.add(jArray.get(i).toString());
                    }
                }

                return listdata;
            }
        } catch (Exception e){
            return null;
        }

        return null;
    }

    public static Dialog loadingDialog(Context ctx) {
        Dialog loading = new Dialog(ctx);
        loading.requestWindowFeature(Window.FEATURE_NO_TITLE);
        loading.setContentView(R.layout.dialog_loading);
        loading.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        loading.setCanceledOnTouchOutside(false);
        loading.setCancelable(false);

        return loading;
    }

    public static String getTokenAccess() {
        Context ctx = XFoodApplication.getInstance();
        SharedPreferences mPreferences = ctx.getSharedPreferences(SessionController.SESSION_PREF_NAME, ctx.MODE_PRIVATE);

        return mPreferences.getString("token", null);
    }

    public static File getOutputMediaFile(String fileName) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + BuildConfig.APPLICATION_ID
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + fileName);

        return mediaFile;
    }

    public static Bitmap getRoundedCornerBitmap(Context context, Bitmap input, int pixels , int w , int h , boolean squareTL, boolean squareTR, boolean squareBL, boolean squareBR  ) {

        Bitmap output = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        final float densityMultiplier = context.getResources().getDisplayMetrics().density;

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, w, h);
        final RectF rectF = new RectF(rect);

        //make sure that our rounded corner is scaled appropriately
        final float roundPx = pixels*densityMultiplier;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);


        //draw rectangles over the corners we want to be square
        if (squareTL ){
            canvas.drawRect(0, h/2, w/2, h, paint);
        }
        if (squareTR ){
            canvas.drawRect(w/2, h/2, w, h, paint);
        }
        if (squareBL ){
            canvas.drawRect(0, 0, w/2, h/2, paint);
        }
        if (squareBR ){
            canvas.drawRect(w/2, 0, w, h/2, paint);
        }


        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(input, 0,0, paint);

        return output;
    }
}