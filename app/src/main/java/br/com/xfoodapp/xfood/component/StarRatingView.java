package br.com.xfoodapp.xfood.component;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.joanzapata.iconify.widget.IconTextView;

import org.w3c.dom.Text;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victorvrp on 19/03/2016.
 */
public class StarRatingView {
    /**
     * Layout attributes
     */
    private Context ctx;
    private Double media;

    public StarRatingView(Context ctx, Double media) {
        this.ctx = ctx;
        this.media = media;
    }

    private List<TextView> getStars() {
        Double restantMedia = media;
        List<TextView> textViewList = new ArrayList<TextView>();
        IconTextView textView;

        for (int i = 0; i < 5; i++) {
            textView = new IconTextView(ctx);

            if (restantMedia >= 1) {
                textView.setText("{fa-star}");
                textView.setTextColor(Color.parseColor("#FFDA44"));
            } else if (restantMedia > 0) {
                textView.setText("{fa-star-half-o}");
                textView.setTextColor(Color.parseColor("#FFDA44"));
            } else {
                textView.setText("{fa-star-o}");
                textView.setTextColor(Color.parseColor("#cccccc"));
            }

            textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 22);

            // layout params
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 5, 0);
            textView.setLayoutParams(params);

            // add to list
            textViewList.add(textView);

            // decrement
            restantMedia--;
        }

        return textViewList;
    }

    public void setIconsInto(LinearLayout container) {
        List<TextView> textViewList = getStars();

        for (TextView textView : textViewList) {
            container.addView(textView);
        }
    }

    /**
     * Formatar média de qualidade para apenas uma casa decimal
     * @return
     */
    public String getValue() {
        DecimalFormat df = new DecimalFormat("0.0");
        return df.format(media);
    }
}
