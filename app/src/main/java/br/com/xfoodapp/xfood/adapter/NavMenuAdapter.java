package br.com.xfoodapp.xfood.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.util.Util;

/**
 * Created by Victorvrp on 18/03/2016.
 */
public class NavMenuAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private ArrayList<Item> mData = new ArrayList<Item>();
    private Context ctx;

    public NavMenuAdapter(Context c) {
        ctx = c;
        mInflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void addItem(String name, String icon) {
        Item item = new Item();
        item.name = name;
        item.icon = icon;

        mData.add(item);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Item getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        if (convertView == null) {
            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.adapter_nav_item, null);
            holder.nameTextView = (TextView) convertView.findViewById(R.id.nav_item_name);
            holder.iconTextView = (TextView) convertView.findViewById(R.id.nav_item_icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.nameTextView.setText(mData.get(position).name);
        holder.iconTextView.setText(mData.get(position).icon);
        return convertView;
    }
}

class Item {
    public String name;
    public String icon;
}

class ViewHolder {
    public TextView nameTextView;
    public TextView iconTextView;
}
