package br.com.xfoodapp.xfood;

import android.app.Application;
import android.graphics.Typeface;
import android.util.Log;

import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import com.loopj.android.http.AsyncHttpClient;

import java.lang.reflect.Field;
import java.util.HashMap;

import br.com.xfoodapp.xfood.model.User;
import br.com.xfoodapp.xfood.util.FontsOverride;

/**
 * Created by Victorvrp on 16/03/2016.
 */
public class XFoodApplication extends Application {
    /**
     * Fonts statics
     */
    private static final String DEFAULT_REGULAR = "montserrat/regular.ttf";
    private static final String DEFAULT_BOLD = "montserrat/bold.ttf";

    /**
     * Instances
     */
    private static XFoodApplication mInstance;
    public static User user;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        // use iconify
        Iconify.with(new FontAwesomeModule());

        // overrride font
        FontsOverride.setDefaultFont(this, "MONOSPACE", DEFAULT_REGULAR);

        // log
        Log.e("oncreateppliaction", "OnCreateApplication");
    }

    public static XFoodApplication getInstance() {
        return mInstance;
    }

    public static AsyncHttpClient http() {
        return new AsyncHttpClient();
    }

    /**
     * Mudar as fonts padrões da aplicação
     */
    private void setDefaultFont() {
        try {
            final Typeface bold = Typeface.createFromAsset(getAssets(), DEFAULT_BOLD);
            final Typeface regular = Typeface.createFromAsset(getAssets(),DEFAULT_REGULAR);
            final Typeface italic = Typeface.createFromAsset(getAssets(), DEFAULT_REGULAR);
            final Typeface boldItalic = Typeface.createFromAsset(getAssets(), DEFAULT_BOLD);

            Field DEFAULT = Typeface.class.getDeclaredField("DEFAULT");
            DEFAULT.setAccessible(true);
            DEFAULT.set(null, regular);

            Field DEFAULT_BOLD = Typeface.class.getDeclaredField("DEFAULT_BOLD");
            DEFAULT_BOLD.setAccessible(true);
            DEFAULT_BOLD.set(null, bold);

            Field sDefaults = Typeface.class.getDeclaredField("sDefaults");
            sDefaults.setAccessible(true);
            sDefaults.set(null, new Typeface[]{
                    regular, bold, italic, boldItalic
            });

        } catch (Throwable e) {
            Log.e("error trhrow", e.getMessage());
        }
    }
}
