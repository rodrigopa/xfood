package br.com.xfoodapp.xfood.helpers;

/**
 * Created by Victorvrp on 16/03/2016.
 */
public class Validator {
    /**
     * Validar um email
     * @param email
     * @return boolean True para um email válido, falso para um inválido
     */
    public static boolean email(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }
}
