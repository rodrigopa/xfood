package br.com.xfoodapp.xfood.http.rest;

import android.content.Context;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;

import br.com.xfoodapp.xfood.util.Util;

/**
 * Created by Victorvrp on 17/03/2016.
 */
public class AuthenticatedHttp {
    /**
     * Class attributes
     */
    protected AsyncHttpClient client;
    protected Context ctx;
    private static final int CONNECTION_TIMEOUT = 20*1000;

    protected AsyncHttpClient getAuthHeader(AsyncHttpClient client) {
        // add authenticate header
        client.addHeader("Authorization", "Bearer " + Util.getTokenAccess());
        return client;
    }

    protected void setupClient() {
        // start aync http client
        client = new AsyncHttpClient();
        client.setTimeout(AuthenticatedHttp.CONNECTION_TIMEOUT);
    }
}
