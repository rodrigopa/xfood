package br.com.xfoodapp.xfood.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.loopj.android.http.RequestParams;

import org.json.JSONObject;

import java.util.Arrays;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.controller.SessionController;
import br.com.xfoodapp.xfood.exception.InvalidFormException;
import br.com.xfoodapp.xfood.helpers.Validator;
import br.com.xfoodapp.xfood.http.JsonHandler;
import br.com.xfoodapp.xfood.util.ApplicationOptions;
import br.com.xfoodapp.xfood.util.Util;
import cz.msebera.android.httpclient.Header;

public class LoginActivity extends AppCompatActivity {
    /**
     * Layout components
     */
    private Button btnLogin;
    private Button btnRegister;
    private EditText etEmail;
    private EditText etPassword;
    private Button btnFacebookLogin;
    private Button btnGoogleLogin;

    /**
     * Class attributes
     */
    private Context ctx;
    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private SessionController sessionController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // facebook sdk start
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);

        // start session controller
        this.sessionController = new SessionController(this);

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {
                Log.e("Accesstokenchanged", String.valueOf(newAccessToken != null));
            }
        };

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                String token = loginResult.getAccessToken().getToken();

                // Fazer requisição para a API, para verificar as credenciais do usuário
                sessionController.attempt(token, ApplicationOptions.FACEBOOK_LOGIN_TYPE);
            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "cancel", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError(FacebookException e) {
                Toast.makeText(LoginActivity.this, "cancel " + e.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        // save context
        this.ctx = this;

        // register layout components
        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnRegister = (Button) findViewById(R.id.btnRegister);
        etEmail = (EditText) findViewById(R.id.email);
        etPassword = (EditText) findViewById(R.id.password);
        btnFacebookLogin = (Button) findViewById(R.id.facebook_login);
        btnGoogleLogin = (Button) findViewById(R.id.google_login);

        // action button at password edit text
        etPassword.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    btnLogin.performClick();
                    return true;
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tentar fazer o login por email e senha
                try {
                    // 1. Pegar dados do formulário
                    String emailValue = etEmail.getText().toString().trim();
                    String passwordValue = etPassword.getText().toString().trim();

                    // 2. Validar os dados fornecidos
                    validate(emailValue, passwordValue);

                    // 3. Fazer requisição para a API, para verificar as credenciais do usuário
                    sessionController.attempt(emailValue, passwordValue);
                } catch (InvalidFormException e) {
                    e.getElem().requestFocus();
                    e.getElem().setError(e.getMessage());
                }
            }
        });

        // event click listener
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // start a register activity
                startActivity(new Intent(ctx, RegisterTypeActivity.class));
            }
        });

        // facebook login
        btnFacebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logOut();
                LoginManager.getInstance().logInWithReadPermissions(((LoginActivity) ctx), Arrays.asList("public_profile, email"));
            }
        });
    }

    /**
     * Validar os dados inseridos pelo usuários nas caixas de texto
     */
    private void validate(String emailValue, String passwordValue) throws InvalidFormException {
        if (!Validator.email(emailValue)) {
            throw new InvalidFormException("Forneça um email válido!", etEmail);
        }

        if (passwordValue.isEmpty()) {
            throw new InvalidFormException("Informe sua senha!", etPassword);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        accessTokenTracker.stopTracking();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
