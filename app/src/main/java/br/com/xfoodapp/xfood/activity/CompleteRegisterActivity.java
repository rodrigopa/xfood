package br.com.xfoodapp.xfood.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ObjectAnimator;

import br.com.xfoodapp.xfood.R;

public class CompleteRegisterActivity extends BaseActivity {
    /**
     * Layout components
     */
    private TextView circleCheck;
    private LinearLayout content;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete_register);

        // save context
        this.ctx = this;

        // register layout components
        circleCheck = (TextView) findViewById(R.id.circleCheck);
        content = (LinearLayout) findViewById(R.id.content);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        // animate components
        int duration = 1000;
        YoYo.with(Techniques.FadeIn).duration(duration).playOn(content);

        // background color
        GradientDrawable bgShape = (GradientDrawable) circleCheck.getBackground();
        ObjectAnimator animColor = ObjectAnimator.ofInt(bgShape, "color", Color.parseColor("#ffffff"), Color.parseColor("#99CC00"));
        animColor.setDuration(duration);
        animColor.setEvaluator(new ArgbEvaluator());
        animColor.start();

        // text color
        ObjectAnimator animTextColor = ObjectAnimator.ofInt(circleCheck, "textColor", Color.parseColor("#99CC00"), Color.parseColor("#ffffff"));
        animTextColor.setDuration(duration);
        animTextColor.setEvaluator(new ArgbEvaluator());
        animTextColor.start();

        // btn login
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
    }
}
