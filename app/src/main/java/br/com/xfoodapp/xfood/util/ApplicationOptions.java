package br.com.xfoodapp.xfood.util;

/**
 * Created by Victorvrp on 19/03/2016.
 */
public class ApplicationOptions {
    public static final int EMAIL_LOGIN_TYPE = 0;
    public static final int FACEBOOK_LOGIN_TYPE = 1;
    public static final int GOOGLE_LOGIN_TYPE = 2;
}
