package br.com.xfoodapp.xfood.http.rest;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.util.Log;
import android.widget.ListView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.xfoodapp.xfood.activity.RegisterActivity;
import br.com.xfoodapp.xfood.adapter.RestaurantsListAdapter;
import br.com.xfoodapp.xfood.http.ApiRoutes;
import br.com.xfoodapp.xfood.http.JsonHandler;
import br.com.xfoodapp.xfood.model.Restaurant;
import br.com.xfoodapp.xfood.util.Util;
import cz.msebera.android.httpclient.Header;

/**
 * Created by Victorvrp on 17/03/2016.
 */
public class RestaurantsHttp extends AuthenticatedHttp {
    /**
     * Class attributes
     */
    private AsyncHttpClient client;
    private Context ctx;

    public RestaurantsHttp(Context ctx) {
        this.ctx = ctx;

        // start aync http client
        client = new AsyncHttpClient();
    }

    public void getRestaurantsByLocation(Location location, String query, final RestaurantsHttp.CallbackIndex callback) {
        // location params
        RequestParams params = new RequestParams();
        params.put("latitude", location.getLatitude());
        params.put("longitude", location.getLongitude());

        if (query != null) {
            params.put("search_query", query);
        }

        client = getAuthHeader(client);
        client.get(ctx, ApiRoutes.LIST_RESTAURANTS, params, new JsonHandler(ctx) {
            @Override
            public void onFinish() {
                callback.finish();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                // retrieving
                ArrayList<Restaurant> restaurantsList = new ArrayList<Restaurant>();
                JSONObject loopObject;
                for (int i = 0; i < response.length(); i++) {
                    try {
                        loopObject = response.getJSONObject(i);

                        // create new object
                        int id = Integer.parseInt(loopObject.getString("id"));
                        String name = loopObject.getString("name");
                        Double distance = loopObject.getDouble("distance");
                        int priceRating = loopObject.getInt("price_rating");

                        Restaurant loopRestaurant = new Restaurant(id, name, priceRating, null, distance);
                        loopRestaurant.setHasFrontImage(loopObject.getBoolean("has_front_image"));

                        if (loopObject.get("quality_rating") != null) {
                            loopRestaurant.setQualityRating(loopObject.getDouble("quality_rating"));
                        }

                        // add to list
                        restaurantsList.add(loopRestaurant);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("Error", e.getMessage());
                    }
                }

                // callback
                callback.index(restaurantsList);
            }
        });
    }

    public void getRestaurant(int id, JsonHandler jsonHandler) {
        Log.e("Teste", ApiRoutes.RESTAURANT_SHOW + id);
        client = getAuthHeader(client);
        client.get(ApiRoutes.RESTAURANT_SHOW + id, jsonHandler);
    }

    /**
     * Callback functions
     */
    public interface CallbackIndex {
        void index(ArrayList<Restaurant> restaurantsList);
        void finish();
    }
}
