package br.com.xfoodapp.xfood.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.faradaj.blurbehind.BlurBehind;
import com.faradaj.blurbehind.OnBlurCompleteListener;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;
import com.nineoldandroids.animation.Animator;

import java.util.ArrayList;

import br.com.xfoodapp.xfood.R;
import br.com.xfoodapp.xfood.adapter.RestaurantsListAdapter;
import br.com.xfoodapp.xfood.component.PriceRatingView;
import br.com.xfoodapp.xfood.component.StarRatingView;
import br.com.xfoodapp.xfood.http.rest.RestaurantsHttp;
import br.com.xfoodapp.xfood.model.Restaurant;
import br.com.xfoodapp.xfood.util.Util;

public class HomeActivity extends BaseActivity {
    /**
     * Layout components
     */
    private ListView listRestaurants;
    private ViewGroup container;
    private View popupRestaurant;
    private SwipeRefreshLayout refreshLayout;
    private Snackbar snackBar;
    private Menu menu;
    private ViewGroup root;

    /**
     * Class attributes
     */
    private RestaurantsHttp restaurantHttp;
    private LayoutInflater mInflater;
    private boolean popupShow = false;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private Location oldLocation;
    private String provider = LocationManager.NETWORK_PROVIDER;
    private Snackbar searchSnackbar;

    // location permission
    private static final int INITIAL_REQUEST = 0;
    private static final String[] LOCATION_PERMS = { Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION };

    /**
     * Time difference threshold set for one minute.
     */
    static final int TIME_DIFFERENCE_THRESHOLD = 1 * 60 * 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // save context
        this.ctx = this;

        // setup view
        setupToolbar();
        setupDrawerLayout();
        updateUserData();

        // instance layout components
        listRestaurants = (ListView) findViewById(R.id.restaurantsList);
        container = (ViewGroup) findViewById(R.id.restaurantsActivity);
        refreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshLayout);
        root = (ViewGroup) findViewById(R.id.restaurantsRootActivity);

        // test
        restaurantHttp = new RestaurantsHttp(this);

        // save inflater object
        mInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // refresh layout listener
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRestaurants(oldLocation, null);
            }
        });
        refreshLayout.post(new Runnable() {
            @Override
            public void run() {
                refreshLayout.setRefreshing(true);
            }
        });

        // Fetch location
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener() {

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {}

            @Override
            public void onProviderEnabled(String provider) {}

            @Override
            public void onProviderDisabled(String provider) {}

            @Override
            public void onLocationChanged(Location location) {
                if (isBetterLocation(oldLocation, location)) {
                    snackBar = Snackbar.make(container, "Localização com precisão de " +  Math.round(location.getAccuracy()) + " metros.", Snackbar.LENGTH_INDEFINITE);
                    snackBar.show();

                    // reload http data with location
                    getRestaurants(location, null);
                }

                oldLocation = location;
            }
        };

        // location
        try {
            oldLocation = locationManager.getLastKnownLocation(provider);
        } catch (SecurityException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!canAccessLocation()) {
                    requestPermissions(LOCATION_PERMS, INITIAL_REQUEST);
                }
            }
        }

        // handle intent
        handleIntent(getIntent());
    }

    private void startTrackingLocation() {
        long minTime = 5 * 1000; // Minimum time interval for update in seconds, i.e. 5 seconds.
        long minDistance = 5; // Minimum distance change for update in meters, i.e. 10 meters.

        try {
            // location updates
            String getProviderName = LocationManager.NETWORK_PROVIDER; // getProviderName();

            if (oldLocation != null) {
                getRestaurants(oldLocation, null);
            } else {
                snackBar = Snackbar.make(container, "Pegando sua localização...", Snackbar.LENGTH_INDEFINITE);
                snackBar.show();
            }

            locationManager.requestLocationUpdates(getProviderName == null ? provider : getProviderName, minTime, minDistance, locationListener);
        } catch (SecurityException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!canAccessLocation()) {
                    requestPermissions(LOCATION_PERMS, INITIAL_REQUEST);
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.list_search, menu);

        // getting icon
        Drawable searchIcon = new IconDrawable(this, FontAwesomeIcons.fa_search)
                .colorRes(android.R.color.white)
                .actionBarSize();

        // Associate searchable configuration with the SearchView
        SearchManager searchManager =
                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView =
                (SearchView) menu.findItem(R.id.search).setIcon(searchIcon).getActionView();
        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    public void getRestaurants(Location location, String query) {
        if (searchSnackbar != null) {
            searchSnackbar.dismiss();
        }

        // get restaurants
        restaurantHttp.getRestaurantsByLocation(location, query, new RestaurantsHttp.CallbackIndex() {
            @Override
            public void finish() {
                // set refreshing to false
                refreshLayout.setRefreshing(false);
            }

            @Override
            public void index(final ArrayList<Restaurant> restaurantsList) {
                // restaurants adapter to result
                final RestaurantsListAdapter adapter = new RestaurantsListAdapter(ctx, restaurantsList);
                listRestaurants.setAdapter(adapter);
                listRestaurants.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (popupShow) return;
                        // get restaurant object
                        Restaurant restaurant = adapter.getItem(position);

                        // start activity
                        Intent intent = new Intent(ctx, RestaurantProfileActivity.class);
                        intent.putExtra("id", restaurant.getId());
                        startActivity(intent);
                    }
                });
                listRestaurants.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                        popupRestaurant = mInflater.inflate(R.layout.popup_restaurant, null);

                        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                                RelativeLayout.LayoutParams.MATCH_PARENT,
                                RelativeLayout.LayoutParams.MATCH_PARENT);
                        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
                        popupRestaurant.setLayoutParams(params);

                        // get restaurant object
                        Restaurant restaurant = adapter.getItem(position);
                        final ImageView image = (ImageView) popupRestaurant.findViewById(R.id.restaurantImage);
                        TextView name = (TextView) popupRestaurant.findViewById(R.id.restaurantName);
                        LinearLayout containerStarRating = (LinearLayout) popupRestaurant.findViewById(R.id.containerStarRating);
                        LinearLayout containerPriceRating = (LinearLayout) popupRestaurant.findViewById(R.id.containerPriceRating);
                        TextView starRatingMedia = (TextView) popupRestaurant.findViewById(R.id.starRatingMedia);
                        StarRatingView starRatingView = new StarRatingView(ctx, restaurant.getQualityRating());
                        PriceRatingView priceRatingView = new PriceRatingView(ctx, restaurant.getPriceRating());

                        // set data to components
                        name.setText(restaurant.getName());
                        starRatingMedia.setText(starRatingView.getValue());
                        starRatingView.setIconsInto(containerStarRating);
                        priceRatingView.setIconsInto(containerPriceRating);

                        if (restaurant.getFrontImage() != null) {
                            Glide.with(ctx).load(restaurant.getFrontImage()).asBitmap().centerCrop().into(new BitmapImageViewTarget(image) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    int w = resource.getWidth(), h = resource.getHeight();
                                    Bitmap roundBitmap =  Util.getRoundedCornerBitmap(ctx, resource, 8, w, h, true, true, false, false);
                                    image.setImageBitmap(roundBitmap);
                                }
                            });
                        } else {
                            Glide.with(ctx).load(restaurant.getProfileImage()).into(image);
                        }

                        // add to container
                        root.addView(popupRestaurant);

                        // set animation
                        YoYo.with(Techniques.FadeIn)
                                .duration(180)
                                .playOn(popupRestaurant);

                        popupRestaurant.findViewById(R.id.restaurantPopup)
                                .setAnimation(AnimationUtils.loadAnimation(ctx, R.anim.zoom_in));

                        refreshLayout.setEnabled(false);

                        // set popup show to true
                        popupShow = true;

                        return false;
                    }
                });
            }
        });
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent pEvent) {
        super.dispatchTouchEvent(pEvent);
        if (pEvent.getAction() == MotionEvent.ACTION_UP && popupShow) {
            YoYo.with(Techniques.ZoomOut)
                    .duration(180)
                    .playOn(popupRestaurant.findViewById(R.id.restaurantPopup));

            YoYo.with(Techniques.FadeOut)
                    .duration(180)
                    .withListener(new Animator.AnimatorListener() {
                        @Override
                        public void onAnimationStart(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            root.removeView(popupRestaurant);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) {
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }
                    })
                    .playOn(popupRestaurant);

            refreshLayout.setEnabled(true);
            popupShow = false;
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        try {
            locationManager.removeUpdates(locationListener);
            snackBar.dismiss();
        }
        catch (SecurityException e) {}
        catch (Exception e) {}
    }

    private boolean canAccessLocation() {
        return (hasPermission(Manifest.permission.ACCESS_FINE_LOCATION));
    }

    private boolean hasPermission(String perm) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return (PackageManager.PERMISSION_GRANTED == checkSelfPermission(perm));
        }

        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch(requestCode) {
            case INITIAL_REQUEST:
                if (!canAccessLocation()) {
                    snackBar = Snackbar.make(container, "Você não nos deu permissão para acessar sua localização :(", Snackbar.LENGTH_INDEFINITE);
                    snackBar.setAction("Tentar denovo", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            snackBar.dismiss();
                            startTrackingLocation();
                        }
                    });
                    snackBar.show();
                } else {
                    startTrackingLocation();
                }

                break;
        }
    }

    /**
     * Get provider name.
     * @return Name of best suiting provider.
     * */
    private String getProviderName() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_LOW); // Chose your desired power consumption level.
        criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
        criteria.setSpeedRequired(true); // Chose if speed for first location fix is required.
        criteria.setAltitudeRequired(false); // Choose if you use altitude.
        criteria.setBearingRequired(false); // Choose if you use bearing.
        criteria.setCostAllowed(false); // Choose if this provider can waste money :-)

        // Provide your criteria and flag enabledOnly that tells
        // LocationManager only to return active providers.
        return locationManager.getBestProvider(criteria, true);
    }

    /**
     * Decide if new location is better than older by following some basic criteria.
     * This algorithm can be as simple or complicated as your needs dictate it.
     * Try experimenting and get your best location strategy algorithm.
     *
     * @param oldLocation Old location used for comparison.
     * @param newLocation Newly acquired location compared to old one.
     * @return If new location is more accurate and suits your criteria more than the old one.
     */
    private boolean isBetterLocation(Location oldLocation, Location newLocation) {
        // If there is no old location, of course the new location is better.
        if(oldLocation == null) {
            return true;
        }

        // Check if new location is newer in time.
        boolean isNewer = newLocation.getTime() > oldLocation.getTime();

        // Check if new location more accurate. Accuracy is radius in meters, so less is better.
        boolean isMoreAccurate = newLocation.getAccuracy() < oldLocation.getAccuracy();
        if(isMoreAccurate && isNewer) {
            // More accurate and newer is always better.
            return true;
        } else if(isMoreAccurate && !isNewer) {
            // More accurate but not newer can lead to bad fix because of user movement.
            // Let us set a threshold for the maximum tolerance of time difference.
            long timeDifference = newLocation.getTime() - oldLocation.getTime();

            // If time difference is not greater then allowed threshold we accept it.
            if(timeDifference > -TIME_DIFFERENCE_THRESHOLD) {
                return true;
            }
        }

        return false;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        handleIntent(intent);
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            if (oldLocation == null) {
                new AlertDialog.Builder(this)
                        .setTitle("Aguarde!")
                        .setMessage("Aguarde enquanto pegamos sua localização!")
                        .setPositiveButton("OK", null)
                        .show();
                return;
            }

            String query = intent.getStringExtra(SearchManager.QUERY);
            menu.findItem(R.id.search).collapseActionView();

            // stop location manager updates
            try {
                locationManager.removeUpdates(locationListener);
            } catch (SecurityException e) {}

            // set refreshing layout
            refreshLayout.setRefreshing(true);

            // remove adapter
            listRestaurants.setAdapter(null);

            // get restaurants
            getRestaurants(oldLocation, query);

            // snackbar
            searchSnackbar = Snackbar.make(container, "Resultados para: '" + query + "'", Snackbar.LENGTH_INDEFINITE);
            searchSnackbar.show();
        } else {
            // track location
            startTrackingLocation();
        }
    }
}
